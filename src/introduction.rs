use base64::engine::general_purpose;
use base64::Engine;
use hex;
use num_bigint::BigInt;
use std::str::from_utf8;
use std::str::FromStr;

/*
    ASCII is a 7-bit encoding standard which allows the representation
    of text using the integers 0-127
*/
pub fn int_to_ascii(ascii_values: Vec<u8>) {
    let ascii_str: String = ascii_values.into_iter().map(|x| x as char).collect();
    println!("{}", ascii_str);
}

/*
    Hexadecimal can be used in such a way to represent ASCII strings
*/
pub fn hex_to_ascii(hex_str: &str) {
    match hex::decode(hex_str) {
        Ok(bytes) => match String::from_utf8(bytes) {
            Ok(ascii_string) => {
                println!("{}", ascii_string);
            }
            Err(_) => {
                eprintln!("Invalid ASCII characters in hex string");
            }
        },
        Err(_) => {
            eprintln!("Invalid hex strings.");
        }
    }
}

/*
    Base64 allows us to represent binary data as an ASCII string using an alphabet of 64 characters.
    One character of a Base64 string encodes 6 binary digits (bits), and so 4 characters of
    Base64 encode three 8-bit bytes.
*/
pub fn hex_to_base64(hex_str: &str) {
    let base64_engine = general_purpose::STANDARD;

    match hex::decode(hex_str) {
        Ok(bytes) => {
            println!("{}", base64_engine.encode(&bytes));
        }
        Err(_) => {
            eprintln!("Invalid hex strings");
        }
    }
}

/*
    Crypto systems like RSA works on numbers, but messages are made up of characters.
*/
pub fn long_integer_to_string(pre_string_int_message: &str) {
    let bigint_from_string = BigInt::from_str(&pre_string_int_message);

    match bigint_from_string {
        Ok(bigint_number) => {
            let bigint_bytes = bigint_number.to_signed_bytes_be();
            match String::from_utf8(bigint_bytes) {
                Ok(message) => {
                    println!("{}", message);
                }

                Err(_) => {
                    eprint!("Invalid string characters in byte string.");
                }
            }
        }

        Err(e) => {
            eprintln!("There was an error parsing the string into BigInt: {}", e);
        }
    }
}

/*
    XOR is a bitwise operator which returns 0 if the bits are the same, and 1 otherwise.
    In textbooks the XOR operator is denoted by ⊕, but in most challenges and
    programming languages you will see the caret ^ used instead.
*/
pub fn xor_string_with_int(string: &str, key: u8) {
    let mut xor_string = String::new();

    for byte in string.as_bytes() {
        xor_string.push((*byte ^ key) as char);
    }

    println!("{}", xor_string);
}

/*
    XOR Properties
    Commutative: A ⊕ B = B ⊕ A
    Associative: A ⊕ (B ⊕ C) = (A ⊕ B) ⊕ C
    Self-Inverse: A ⊕ A = 0
    Identity: A ⊕ 0 = A

    Challenge: Use the above properties to undo the encryption in the final line to
    obtain the flag. Be sure to decode from hex to bytes.

    KEY1 = a6c8b6733c9b22de7bc0253266a3867df55acde8635e19c73313
    KEY2 ^ KEY1 = 37dcb292030faa90d07eec17e3b1c6d8daf94c35d4c9191a5e1e
    KEY2 ^ KEY3 = c1545756687e7573db23aa1c3452a098b71a7fbf0fddddde5fc1
    FLAG ^ KEY1 ^ KEY3 ^ KEY2 = 04ee9855208a2cd59091d04767ae47963170d1660df7f56f5faf

    --My Mathematical Solution:--

    Using Associative Property:
    (KEY1) ^ (KEY2 ^ KEY3) => LINE1 ^ LINE3 => FLAG ^ KEY123 = COMBINE_KEY
    Using Self-Inverse Property:
    FLAG ^ (KEY123 ^ KEY123) = COMBINE_KEY ^ KEY123
    FLAG ^ 0 = COMBINE_KEY ^ KEY123
    Using Identity Property:
    FLAG = COMBINE_KEY ^ KEY123

    Therefore:
    FLAG = (LINE1 ^ LINE3) ^ LINE4
*/
pub fn xor_properties(key1: &str, key2_3: &str, all_keys: &str) {
    let max_input = i16::MAX as usize;

    let len_result = key1.len() == key2_3.len()
        && key1.len() == all_keys.len()
        && !key1.is_empty()
        && !key2_3.is_empty()
        && !all_keys.is_empty()
        && key1.len() <= max_input;

    if len_result {
        match hex::decode(key1) {
            Ok(bytes1) => {
                let key1_bytes = bytes1;

                match hex::decode(key2_3) {
                    Ok(bytes2) => {
                        let key2_3_bytes = bytes2;

                        let xor_bytes1: Vec<u8> = key1_bytes
                            .iter()
                            .zip(key2_3_bytes.iter())
                            .map(|(&b1, &b2)| b1 ^ b2)
                            .collect();

                        match hex::decode(all_keys) {
                            Ok(bytes3) => {
                                let all_keys_bytes = bytes3;

                                let xor_bytes2: Vec<u8> = xor_bytes1
                                    .iter()
                                    .zip(all_keys_bytes.iter())
                                    .map(|(&b3, &b4)| b3 ^ b4)
                                    .collect();

                                let xor_result = from_utf8(&xor_bytes2);

                                match xor_result {
                                    Ok(flag) => {
                                        println!("{}", flag);
                                    }
                                    Err(_) => {
                                        eprintln!("Error occurred with xor operation of final set");
                                    }
                                }
                            }
                            Err(_) => {
                                eprintln!("Error occurred when decoding the combined key from hex string to bytes");
                            }
                        }
                    }

                    Err(_) => {
                        eprintln!(
                            "Error occurred when decoding second key from hex string to bytes"
                        );
                    }
                }
            }
            Err(_) => {
                eprintln!("Error occurred when decoding first key from hex string to bytes");
            }
        }
    } else {
        eprintln!("Strings must have the same length");
    }
}
