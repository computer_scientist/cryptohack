mod introduction;

// Introduction

fn main() {
    println!("Introduction:");

    introduction::int_to_ascii(vec![
        99, 114, 121, 112, 116, 111, 123, 65, 83, 67, 73, 73, 95, 112, 114, 49, 110, 116, 52, 98,
        108, 51, 125,
    ]);

    introduction::hex_to_ascii(
        "63727970746f7b596f755f77696c6c5f62655f776f726b696e675f776974685f6865785f737472696e67735f615f6c6f747d"
    );

    introduction::hex_to_base64("72bca9b68fc16ac7beeb8f849dca1d8a783e8acf9679bf9269f7bf");

    introduction::long_integer_to_string(
        "11515195063862318899931685488813747395775516287289682636499965282714637259206269",
    );

    introduction::xor_string_with_int("label", 13);

    introduction::xor_properties(
        "a6c8b6733c9b22de7bc0253266a3867df55acde8635e19c73313",
        "c1545756687e7573db23aa1c3452a098b71a7fbf0fddddde5fc1",
        "04ee9855208a2cd59091d04767ae47963170d1660df7f56f5faf",
    );
}
